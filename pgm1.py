from flask import Flask
import socket

app = Flask(__name__)

@app.route("/")
def hello():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    print(s.getsockname()[0])
    ip = s.getsockname()[0]
    s.close()
    return "New version Welcome to my site. I am "+ip

if __name__=='__main__':
	app.run(debug=True, host='127.0.0.1')


